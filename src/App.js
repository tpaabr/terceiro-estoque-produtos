import React, { Component } from 'react';
import './App.css';
import CadastrarProduto from './component/CadastrarProduto'
import Estoque from './component/Estoque'

class App extends React.Component {
  constructor(props){
    super(props);
  }

  render() {
    return (
      <React.Fragment>
      <div>
        <h1>Sistema de Gerenciamento de Produtos</h1>
        <Estoque/>
      </div>
        
      </React.Fragment>
    );
  }
}

export default App;
