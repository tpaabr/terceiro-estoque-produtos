import React from 'react';
import '../styles/cadastrarProduto.css'

class CadastrarProduto extends React.Component{

    constructor(props){
        super(props)
        this.state = {
            nomeProduto: "",
            valorProduto: 0,
            quantidadeProduto: 0
        }
    }

    handleChange = event => {
        if (event.target.name === "nomeProduto"){
            this.setState({
                nomeProduto: event.target.value,
                valorProduto: this.state.valorProduto
            })
        }else if(event.target.name === "valorProduto"){
            this.setState({
                nomeProduto: this.state.nomeProduto,
                valorProduto: event.target.value
            })
        }
    }

    handleSubmit = (event) => {
        event.preventDefault();
        let produtoAtual = this.state;
        this.props.addProduto(produtoAtual);
        
    }

    render(){
        return(
            <React.Fragment>
                <form class="formCadastro">
                    <label class="labelCadastro">Nome do Produto:</label>
                    <input class="inputCadastro"
                        type="text"
                        name="nomeProduto"
                        onChange={this.handleChange}
                    />
                    <label class="labelCadastro">Preço do Produto:</label>
                    <input class="inputCadastro"
                        type="number"
                        name="valorProduto"
                        onChange={this.handleChange}
                    />

                    <button class="buttonCadastro" onClick={this.handleSubmit}>Cadastar Produto</button>
                </form>
            </React.Fragment>
        );
    }
}

export default CadastrarProduto;