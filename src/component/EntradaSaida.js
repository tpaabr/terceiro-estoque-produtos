import React from 'react';
import '../styles/entradaSaida.css'

class EntradaSaida extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            lista: this.props.listaProdutos,
            nomeProdutoAtual: "",
            novaQuantidade: 0,
            
        }
    }

    handleQuantidade = (event) => {
        this.setState({
            lista: this.props.listaProdutos, 
            nomeProdutoAtual: this.state.nomeProdutoAtual, 
            novaQuantidade: event.target.value,
            
        })

    }

    handleSelect = event => {
        
        console.log(event.target.value)
        this.setState({
            lista: this.props.listaProdutos,
            nomeProdutoAtual: event.target.value,
            novaQuantidade: this.state.novaQuantidade,
            
        })
    }

    handleSubmit = event => {

        event.preventDefault();
        let nome = this.state.nomeProdutoAtual;
        let quantidade = parseInt(this.state.novaQuantidade);

        //Impedindo adicionar/remover quantidades de produto inexistente
        if (nome === ""){
            alert("Escolha um produto antes da operação")
        }else{

            if(event.target.name === "entrada"){
                this.props.atualizarQuantidade(nome, quantidade, true);
            }else if(event.target.name === "saida"){
                console.log(nome)
                this.props.atualizarQuantidade(nome, quantidade, false);
            }
            
        }



    }

    render(){
        return(
            <React.Fragment>
                <form class="formEntradaSaida">
                    <select class="selectQuantidade" onChange={this.handleSelect}>
                        <option>Selecione Um Produto</option>
                        {this.state.lista.map((produto, index) => <option key={index} value={produto.nomeProduto} >{produto.nomeProduto}</option>)}
                    </select>
                    <input class="inputQuantidade"
                        type="number"
                        name="quantidade" 
                        onChange={this.handleQuantidade}
                    />
                    <button class="butQuantidade" onClick={this.handleSubmit} name="entrada">Entrar com essa quantidade</button>
                    <button class="butQuantidade" onClick={this.handleSubmit} name="saida">Sair com essa quantidade</button>
                </form>
            </React.Fragment>
        );
        
    }
}

export default EntradaSaida;