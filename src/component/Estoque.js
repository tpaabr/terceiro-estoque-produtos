import React from 'react';
import CadastrarProduto from './CadastrarProduto';
import EntradaSaida from './EntradaSaida';
import '../styles/estoque.css'

class Estoque extends React.Component{
    constructor(props){
        super(props)
        this.state = {
            lista: []
        }

        this.lista = []
    }

    addProduto = (produto) => {

        let existe = false

        //Não deixar criar produto com mesmo nome
        this.state.lista.forEach(element => {
            if(element.nomeProduto === produto.nomeProduto){
                alert("Já existe um produto com esse nome")
                existe = true
            }
        });

        if(!existe){
            let novaLista = this.state.lista
            novaLista[novaLista.length] = produto
            
            this.setState({lista: novaLista})
        }

        this.render();
    }

    atualizarQuantidade = (nomeProduto, quantidade, entrada) => {
        //Entrada true para adicionar quantidade de produtos, false para diminuir
        let novaLista = this.state.lista;
        for (let i = 0; i < this.state.lista.length; i++) {
            //Achar index do produto pelo seu nome
            if(this.state.lista[i].nomeProduto === nomeProduto){
                var index = i;
            }  
        }

        //Tipo de operação
        if(entrada){
            novaLista[index].quantidadeProduto += quantidade;
        }else{
            let quantidadeAtual = novaLista[index].quantidadeProduto;
            if(quantidadeAtual - quantidade <= 0){
                novaLista.splice(index, 1)
                console.log(novaLista)
            }else{
                novaLista[index].quantidadeProduto += -quantidade;
            }
        }

        this.setState({lista: novaLista});
    }

    render(){
        if(this.state.lista.length > 0){
            return(
                <div class="estoque">
                    <CadastrarProduto addProduto={this.addProduto}/>
                    <h2 class="avisu">Mudar Quantidade de Produtos:</h2>
                    <EntradaSaida listaProdutos={this.state.lista} atualizarQuantidade={this.atualizarQuantidade}/>
                    <div class="listaProdutos">
                        {this.state.lista.map((produto, index) => <p class="produto" key={index} >Nome Produto:{produto.nomeProduto} || Preço: {produto.valorProduto} || Quantidade: {produto.quantidadeProduto}</p>)}
                    </div>
                </div>
            );
        }else{
            return(
                <div class="estoque">
                    <CadastrarProduto addProduto={this.addProduto}/>
                    <h2 class="aviso">Adicione um PRODUTO</h2>
                </div>
            );
        }
    }
}

export default Estoque;
